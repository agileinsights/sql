-- DELETE FROM DIAL_POP_DATA_FACT where source_id = 2;

CREATE TABLE DIAL_SQL_REQUEST_FACT 
(
        REQUEST_ID VARCHAR2(25), 
        SQL_STRING CLOB, 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25)
);

CREATE TABLE DIAL_POP_DATA_FACT 
(
        SOURCE_ID NUMBER, 
        SUBJECT_ID VARCHAR2(25), 
        SUBJECT_ID_SOURCE VARCHAR2(25), 
        SUBJECT_JSON VARCHAR2(8000), 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25), 
CONSTRAINT ENSURE_JSON CHECK (subject_json IS JSON)
);

CREATE INDEX DIAL_POP_DATA_FACT_IDX 
   ON DIAL_POP_DATA_FACT (SOURCE_ID, SUBJECT_ID);

DROP TABLE DIAL_POP_DATA_SOURCE_DIM;

CREATE TABLE DIAL_POP_DATA_SOURCE_DIM
(
        SOURCE_ID NUMBER, 
        SOURCE_NM VARCHAR2(100), 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25)
);

CREATE INDEX DIAL_POP_DATA_SOURCE_DIM_IDX 
   ON DIAL_POP_DATA_SOURCE_DIM (SOURCE_ID);
   
///////////

CREATE TABLE PROGRAM_FACT
(
        PROGRAM_ID NUMBER, 
        PROGRAM_NM VARCHAR2(100),
        hierarchy VARCHAR2(100), 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25)
);

ALTER TABLE PROGRAM_FACT
  ADD hierarchy VARCHAR2(100); 
  
update PROGRAM_FACT set hierarchy = 'organizational'

CREATE INDEX PROGRAM_FACT_IDX 
   ON PROGRAM_FACT (PROGRAM_ID);

insert into PROGRAM_FACT (PROGRAM_ID, PROGRAM_NM, INSERT_DATE, INSERT_BY)
 values (2, 'Demo', CURRENT_DATE, '3173879');
delete from program_fact where program_id = 2
   
CREATE TABLE PROGRAM_METRIC_DIM_FACT
(
        PROGRAM_ID NUMBER, 
        FACT_TYPE VARCHAR2(100), 
        METRIC_DIM_ID NUMBER, 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25)
);

CREATE INDEX PROGRAM_METRIC_DIM_FACT_IDX 
   ON PROGRAM_METRIC_DIM_FACT (PROGRAM_ID, FACT_TYPE, METRIC_DIM_ID);

insert into PROGRAM_METRIC_DIM_FACT (PROGRAM_ID, FACT_TYPE, METRIC_DIM_ID, INSERT_DATE, INSERT_BY)
select 1, 'metric', 16, CURRENT_DATE, '3173879' from dual --metric_data_dim where metric_id < 5;

insert into PROGRAM_METRIC_DIM_FACT (PROGRAM_ID, FACT_TYPE, METRIC_DIM_ID, INSERT_DATE, INSERT_BY)
select 2, 'dimension', dimension_id, CURRENT_DATE, '3173879' from dimension_data_dim where dimension_id < 2;

select program_id, metric_cd, metric_label
				from metric_data_dim m 
				inner join program_metric_dim_fact p
				on m.metric_id = p.metric_dim_id 
				and p.fact_type = 'metric' 
				and p.program_id = 1 
				order by metric_cd;


//////////////
   insert into DIAL_POP_DATA_SOURCE_DIM (SOURCE_ID, SOURCE_NM, INSERT_DATE, INSERT_BY)
 values (6, 'Patient Portal', CURRENT_DATE, '3173879');
 
 -- create metric fact table
 CREATE TABLE METRIC_DATA_FACT 
(
        metric_ID NUMBER, 
        SUBJECT_ID VARCHAR2(25), 
        SUBJECT_ID_SOURCE VARCHAR2(25), 
        PARENT_ID VARCHAR2(25), 
        PARENT_ID_SOURCE VARCHAR2(25), 
        POST_DATE DATE, 
        METRIC_CD VARCHAR2(100), 
        METRIC_VALUE NUMBER, 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25) 
);


CREATE INDEX METRIC_DATA_FACT_IDX 
   ON METRIC_DATA_FACT (metric_cd, SUBJECT_ID, PARENT_ID, POST_DATE);

CREATE INDEX METRIC_DATA_FACT_ID_IDX 
   ON METRIC_DATA_FACT (METRIC_ID);
   
CREATE SEQUENCE metric_id_seq
 START WITH     17
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

update metric_data_dim m
set m.metric_id = (select id from temp_id i where m.metric_cd = i.metric_cd)

update dimension_data_dim d
set d.dimension_id = (select id from temp_id i where d.dimension_cd = i.dimension_cd)

update dimension_data_fact f
set f.dimension_id = (select dimension_id from dimension_data_dim d where d.dimension_combo_id = f.dimension_combo_id)
   
update metric_data_fact f
set f.metric_id = (select metric_id from metric_data_dim m where m.metric_cd = f.metric_cd)

 -- create dimesion fact table
 CREATE TABLE DIMENSION_DATA_FACT 
(
        dimension_ID NUMBER, 
        SUBJECT_ID VARCHAR2(25), 
        SUBJECT_ID_SOURCE VARCHAR2(25), 
        PARENT_ID VARCHAR2(25), 
        PARENT_ID_SOURCE VARCHAR2(25), 
        POST_DATE DATE, 
        DIMENSION_COMBO_ID NUMBER, 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25) 
);

CREATE INDEX DIMENSION_DATA_FACT_IDX 
   ON DIAL_POP_DATA_FACT (dimension_ID, SUBJECT_ID, PARENT_ID, POST_DATE);
CREATE INDEX DIMENSION_COMBO_IDX 
   ON DIAL_POP_DATA_FACT (DIMENSION_COMBO_ID);
   
 -- create dimesion dim table
 CREATE TABLE DIMENSION_DATA_DIM 
(
        dimension_ID NUMBER, 
        DIMENSION_COMBO_ID NUMBER, 
        DIMENSION_CD VARCHAR2(100), 
        DIMENSION_VALUE VARCHAR(1000), 
        DIMENSION_COMBO VARCHAR(1200), 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25) 
);

CREATE INDEX DIMENSION_DATA_DIM_IDX 
   ON DIMENSION_DATA_DIM (SOURCE_ID, DIMENSION_COMBO_ID);
   
CREATE SEQUENCE DIMENSION_COMBO_ID_SEQ
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 -- create metric dim table

CREATE TABLE METRIC_DATA_DIM 
(
        metric_ID NUMBER, 
        METRIC_CD VARCHAR2(100), 
        METRIC_LABEL VARCHAR2(100), 
        METRIC_TYPE VARCHAR2(100), 
        METRIC_NUMERATOR VARCHAR2(100), 
        METRIC_DENOMINATOR VARCHAR2(100), 
        METRIC_DESCRIPTION VARCHAR2(1000), 
        INSERT_DATE DATE, 
        INSERT_BY VARCHAR2(25) 
);

CREATE INDEX METRIC_DATA_DIM_IDX 
   ON METRIC_DATA_DIM (SOURCE_ID, METRIC_CD);
   
ALTER TABLE METRIC_DATA_DIM
  RENAME COLUMN SOURCE_ID to DIMENSION_ID;

// UPDATE METRIC_DATA_DIM
// set metric_denominator = 1

delete from dial_sql_request_fact

delete from METRIC_DATA_DIM where metric_cd = 'ADOPT'

update METRIC_DATA_DIM 
set 
METRIC_DESCRIPTION = 'The total number of vital alerts generated.'
where metric_cd = 'VITAL_ALERT_IND'

ALTER TABLE METRIC_DATA_DIM
--  RENAME COLUMN METRIC_TYPE to METRIC_CALC;
--  ADD METRIC_PREFIX varchar(5);
--  ADD METRIC_SUFFIX varchar(5);
--  ADD METRIC_SCALE NUMBER;
--  ADD METRIC_DECIMAL NUMBER; 
--  ADD METRIC_GOAL_LOW NUMBER; 
--  ADD METRIC_GOAL_MED NUMBER; 
--  ADD METRIC_GOAL_HIGH NUMBER;
--  ADD METRIC_SOURCE varchar(25); 

update metric_data_DIM set metric_denominator = 'IS_ENROLLED_IND' where metric_cd = 'REG_ENROLL_RATIO'


'ENROLL_REG'
'ENROLL_REG'

insert into METRIC_DATA_DIM (
        METRIC_ID, 
        METRIC_CD, 
        METRIC_LABEL, 
        METRIC_CALC, 
        METRIC_NUMERATOR, 
        METRIC_DENOMINATOR, 
        METRIC_DESCRIPTION, 
        INSERT_DATE, 
        INSERT_BY,
        METRIC_TYPE,
        METRIC_GROUP,
        METRIC_PREFIX,
        METRIC_SUFFIX,
        METRIC_SCALE,
        METRIC_UP_MEANING,
        METRIC_DECIMAL
        )
        values (
        16,
        'REG_ENROLL_RATIO',
        'Ratio Enroll Ratio',
        'Rate',
        'ENROLL_REG',
        'IS_ENROLLED',
        'Ratio of registered to enrolled patients.',
        CURRENT_DATE,
        'Anthony',
        'Actual',
        'REG_ENROLL_RATIO',
        null,
        '%',
        100,
        'good',
        2  
        )
        

-- create function to read webservices   
--/
CREATE OR REPLACE FUNCTION readwebservice(url VARCHAR2)

RETURN CLOB
IS
   pcs UTL_HTTP.html_pieces;
   retv CLOB;
BEGIN
   pcs := UTL_HTTP.request_pieces(url, 5000);
   FOR i IN 1 .. pcs.COUNT
   LOOP
      retv := retv || pcs (i);
   END LOOP;
   RETURN retv;
END;
/

-- sql to call web service
SELECT jt.*
from (
    select 
    readwebservice('http://10.12.35.92/datasource.php?request_id=' || <Parameters.pRequestID>) as jstring
    from dual
) jdata,
JSON_TABLE(jstring, '$.rows[*]'
COLUMNS (
        POST_DATE VARCHAR2(20) PATH '$.POST_DATE',
        METRIC_CD VARCHAR2(20) PATH '$.METRIC_CD',
        METRIC_GROUP VARCHAR2(20) PATH '$.METRIC_GROUP',
        METRIC_RANK VARCHAR2(20) PATH '$.METRIC_RANK',
        METRIC_TYPE VARCHAR2(20) PATH '$.METRIC_TYPE',
        METRIC_CALC VARCHAR2(20) PATH '$.METRIC_CALC',
        METRIC_LABEL VARCHAR2(20) PATH '$.METRIC_LABEL',
        METRIC_PARAM NUMBER PATH '$.METRIC_PARAM',
        METRIC_DESCRIPTION VARCHAR2(500) PATH '$.METRIC_DESCRIPTION',
        METRIC_PREFIX VARCHAR2(20) PATH '$.METRIC_PREFIX',
        METRIC_SUFFIX VARCHAR2(20) PATH '$.METRIC_SUFFIX',
        METRIC_SCALE NUMBER PATH '$.METRIC_SCALE',
        METRIC_DECIMAL NUMBER PATH '$.METRIC_DECIMAL',
        METRIC_UP_MEANING VARCHAR2(20) PATH '$.METRIC_UP_MEANING',
        GROUPBY1_CD VARCHAR2(20) PATH '$.GROUPBY1_CD',
        GROUPBY1_VALUE VARCHAR2(20) PATH '$.GROUPBY1_VALUE',
        NUMERATOR NUMBER PATH '$.NUMERATOR',
        DENOMINATOR NUMBER PATH '$.DENOMINATOR',
        NUMERATOR_GOAL NUMBER PATH '$.NUMERATOR_GOAL',
        DENOMINATOR_GOAL NUMBER PATH '$.DENOMINATOR_GOAL',
        GOAL_FLAG NUMBER PATH '$.GOAL_FLAG'

)) AS jt

-- create function to parse bit id to specific level of hierarchy
-- v = concatenated id, p = position, s = length of id
--/
CREATE OR REPLACE FUNCTION hierlevel(v IN number, p IN number, s IN number)

RETURN number
IS 
   retv number;
BEGIN
   retv := (mod(v, power(10,((p+1)*s))) - mod(v, power(10,(p*s)))) / power(10,(p*s));
   RETURN retv;
END;
/

--------------------------------------------------------
--- crate pipeline function to build dynamic queries ---
--------------------------------------------------------

-- define an object type
create or replace TYPE datasetObject AS OBJECT (

   	post_date date, 
   	METRIC_CD VARCHAR2(50), 
   	metric_calc VARCHAR2(25), 
   	metric_group VARCHAR2(25), 
   	metric_rank VARCHAR2(25), 
   	metric_description VARCHAR2(500), 
   	metric_label VARCHAR2(25), 
   	metric_param number(18,6),
	metric_prefix VARCHAR2(25), 
	metric_suffix VARCHAR2(25), 
	metric_scale number(18,6), 
	metric_up_meaning VARCHAR2(25), 
	metric_decimal number(18,6), 
	metric_goal_low number(18,6), 
	metric_goal_med number(18,6), 
	metric_goal_high number(18,6), 
	metric_source VARCHAR2(25), 
	GROUPBY1_CD VARCHAR2(25), 
	GROUPBY1_VALUE VARCHAR2(250),
	numerator number(18,6),
	denominator number(18,6),
	numerator_goal number(18,6),
	denominator_goal number(18,6),
	goal_flag number(18,6)
   
   );


-- drop TYPE datasetObject_ntt;
-- define type "table" of MyObjects
create TYPE datasetObject_ntt IS TABLE OF datasetObject ;

-- define the function which would cookup SQL and return a cursor of MyObjects
--/
    create or replace FUNCTION get_datasetObjects(p_request_id VARCHAR2) RETURN datasetObject_ntt 
    PIPELINED
    PARALLEL_ENABLE
    IS
    sqlcur sys_refcursor ;
    cur sys_refcursor ;
    post_date date ;
   	METRIC_CD VARCHAR2(50) ;
   	metric_calc VARCHAR2(25) ;
   	metric_group VARCHAR2(25) ;
   	metric_rank VARCHAR2(25) ;
   	metric_description VARCHAR2(500) ;
   	metric_label VARCHAR2(25) ;
   	metric_param number(18,6) ;
	metric_prefix VARCHAR2(25) ;
	metric_suffix VARCHAR2(25) ;
	metric_scale number(18,6) ;
	metric_up_meaning VARCHAR2(25) ;
	metric_decimal number(25) ;
	metric_goal_low number(18,6); 
	metric_goal_med number(18,6); 
	metric_goal_high number(18,6); 
	metric_source VARCHAR2(25); 
	GROUPBY1_CD VARCHAR2(25) ;
	GROUPBY1_VALUE VARCHAR2(250) ;
	numerator number(18,6) ;
	denominator number(18,6) ;
	numerator_goal number(18,6) ;
	denominator_goal number(18,6) ;
	goal_flag number(18,6) ;
    sqlrequest varchar2(20000); 
    BEGIN  
        OPEN sqlcur FOR 'SELECT  dbms_lob.substr(sql_string, 20000, 1)  FROM DIAL_SQL_REQUEST_FACT where request_id = ''' || p_request_id || '''';
        FETCH sqlcur INTO sqlrequest;
        
        OPEN cur FOR sqlrequest;
        LOOP
            FETCH cur INTO
                post_date, 
                METRIC_CD, 
                metric_calc, 
                metric_group, 
                metric_rank, 
                metric_description, 
                metric_label, 
                metric_param,
                metric_prefix, 
                metric_suffix, 
                metric_scale, 
                metric_up_meaning, 
                metric_decimal, 
		metric_goal_low, 
		metric_goal_med, 
		metric_goal_high, 
		metric_source, 
                GROUPBY1_CD, 
                GROUPBY1_VALUE,
                numerator,
                denominator,
                numerator_goal,
                denominator_goal,
                goal_flag
             ;
            EXIT WHEN cur% notfound ;
            PIPE ROW(NEW datasetObject(
                post_date, 
                METRIC_CD, 
                metric_calc, 
                metric_group, 
		metric_rank, 
                metric_description, 
                metric_label, 
                metric_param,
                metric_prefix, 
                metric_suffix, 
                metric_scale, 
                metric_up_meaning, 
                metric_decimal, 
		metric_goal_low, 
		metric_goal_med, 
		metric_goal_high, 
		metric_source, 
                GROUPBY1_CD, 
                GROUPBY1_VALUE,
                numerator,
                denominator,
                numerator_goal,
                denominator_goal,
                goal_flag
            ));
        END LOOP ;
        RETURN;
    END ;
/

select * from TABLE(get_datasetObjects('58484075f411f'));
Select * from user_errors --where name='get_datasetObjects'


select subject_id, subject_json
from dial_pop_data_fact
where source_id = 5


-- create facility hierarchy view
create or replace view fac_dial_hierarchy as

select org_div_global_id || org_ops_grp_global_id || org_regn_global_id || org_area_global_id || lpad(fac_id, 6, '0') as bit_id,
lpad(fac_id, 6, '0') as global_id, fac_id as id, fac_nm as name, org_area_id as parent_id, org_area_global_id as parent_global_id, 'facility' as "level", 0 as level_position, 
'area' as parent_level, null as child_level, 'organizational' as hierarchy
from ORG.FAC_DIAL
where fac_stat_cd = 'Active'

union
select org_div_global_id || org_ops_grp_global_id || org_regn_global_id || org_area_global_id as bit_id,
org_area_global_id as global_id, org_area_id as id, org_area_nm as name, org_regn_id as parent_id, org_regn_global_id as parent_global_id, 'area' as "level", 1 as level_position, 
'region' as parent_level, 'facility' as child_level, 'organizational' as hierarchy
from ORG.FAC_DIAL
where fac_stat_cd = 'Active'

union
select org_div_global_id || org_ops_grp_global_id || org_regn_global_id as bit_id,
org_regn_global_id as global_id, org_regn_id as id, org_regn_nm as name, org_ops_grp_id as parent_id, org_ops_grp_global_id as parent_global_id, 'region' as "level", 2 as level_position, 
'group' as parent_level, 'area' as child_level, 'organizational' as hierarchy
from ORG.FAC_DIAL
where fac_stat_cd = 'Active'

union
select org_div_global_id || org_ops_grp_global_id as bit_id,
org_ops_grp_global_id as global_id, org_ops_grp_id as id, org_ops_grp_nm as name, org_div_id as parent_id, org_div_global_id as parent_global_id, 'group' as "level", 3 as level_position, 
'division' as parent_level, 'region' as child_level, 'organizational' as hierarchy
from ORG.FAC_DIAL
where fac_stat_cd = 'Active'

union
select org_div_global_id as bit_id,
org_div_global_id as global_id, org_div_id as id, org_div_nm as name, org_co_id as parent_id, org_co_global_id as parent_global_id, 'division' as "level", 4 as level_position, 
'company' as parent_level, 'group' as child_level, 'organizational' as hierarchy
from ORG.FAC_DIAL
where fac_stat_cd = 'Active'

-- ESCO Heirarchy

union
select '99' || substr(esco_id, 2, 4) || lpad(e.fac_id, 6, '0') as bit_id,
lpad(e.fac_id, 6, '0') as global_id, e.fac_id as id, o.fac_nm as name, esco_id as parent_id,  '99' || substr(esco_id, 2, 4) as parent_global_id, 'facility' as "level", 0 as level_position, 
'market' as parent_level, null as child_level, 'ESCO' as hierarchy
from (
        SELECT distinct
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_ID') as ESCO_ID,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_MARKET') as ESCO_MARKET,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPCLINICNUM') as fac_id
        FROM "BI_SRC"."DIAL_POP_DATA_FACT"
        where DIAL_POP_DATA_FACT.source_id in (5)
        ) e
                left join ORG.FAC_DIAL o on e.fac_id = o.fac_id
                and o.fac_stat_cd = 'Active'

union 
select distinct '99' || substr(esco_id, 2, 4) as bit_id,
'99' || substr(esco_id, 2, 4) as global_id, esco_id as id, ESCO_MARKET as name, '2800' as parent_id,  '100002' as parent_global_id, 'market' as "level", 1 as level_position, 
'company' as parent_level, 'facility' as child_level, 'ESCO' as hierarchy
from (
        SELECT distinct
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_ID') as ESCO_ID,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_MARKET') as ESCO_MARKET,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPCLINICNUM') as fac_id
        FROM "BI_SRC"."DIAL_POP_DATA_FACT"
        where DIAL_POP_DATA_FACT.source_id in (5)
        ) e

	
	
create view Z_ESCO_FAC_DIM as
        SELECT distinct
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_ID') as ESCO_ID,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_MARKET') as ESCO_MARKET,
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPCLINICNUM') as fac_id
        FROM "BI_SRC"."DIAL_POP_DATA_FACT"
        where DIAL_POP_DATA_FACT.source_id in (5)